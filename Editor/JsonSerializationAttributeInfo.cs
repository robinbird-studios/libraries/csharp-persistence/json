﻿#region Disclaimer
// <copyright file="JsonSerializationAttributeInfo.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_EDITOR

namespace RobinBird.Persistence.Json.Editor
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Provides Information about the Json serializer
    /// </summary>
    public class JsonSerializationAttributeInfo : ISerializationAttributeInfo
    {
        public Type GetIgnoreAttributeType()
        {
            return typeof (JsonIgnoreAttribute);
        }
    }
}

#endif