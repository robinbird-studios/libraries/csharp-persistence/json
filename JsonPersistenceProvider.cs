﻿#region Disclaimer

// <copyright file="JsonPersistenceProvider.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

using System.Text;

namespace RobinBird.Persistence.Json
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Newtonsoft.Json;
    using Runtime;

    public class JsonPersistenceProvider : IPersistenceProvider
    {
        private static JsonPersistenceProvider indentedProvider;
        private static JsonPersistenceProvider fastProvider;

        private static List<JsonConverter> globalConverters;

        private readonly Newtonsoft.Json.JsonSerializer serializer;

        public JsonPersistenceProvider(Newtonsoft.Json.JsonSerializer serializer)
        {
            // Adding global converters
            if (globalConverters != null)
            {
                for (var i = 0; i < globalConverters.Count; i++)
                {
                    JsonConverter converter = globalConverters[i];
                    if (converter == null)
                    {
                        continue;
                    }
                    serializer.Converters.Add(converter);
                }
            }
            this.serializer = serializer;
        }

        /// <summary>
        /// Template provider that is indented but does not handle references. Best use for readable/mergeable files.
        /// </summary>
        public static JsonPersistenceProvider IndentedProvider
        {
            get
            {
                if (indentedProvider == null)
                {
                    var settings = new JsonSerializerSettings
                    {
                        Formatting = Formatting.Indented,
                    };
                    indentedProvider = new JsonPersistenceProvider(Newtonsoft.Json.JsonSerializer.Create(settings));
                }
                return indentedProvider;
            }
        }
        
        /// <summary>
        /// Template provider that is fast and small
        /// </summary>
        public static JsonPersistenceProvider FastProvider
        {
            get
            {
                if (fastProvider == null)
                {
                    var settings = new JsonSerializerSettings
                    {
                    };
                    fastProvider = new JsonPersistenceProvider(Newtonsoft.Json.JsonSerializer.Create(settings));
                }
                return fastProvider;
            }
        }

        public static void AddGlobalConverter(JsonConverter converter)
        {
            if (globalConverters == null)
            {
                globalConverters = new List<JsonConverter>();
            }
            globalConverters.Add(converter);
        }

        #region IPersistenceProvider Implementation

        public string Extension
        {
            get { return "json"; }
        }

        public void Serialize(Stream s, object o, Type t)
        {
            // Make sure stream is not closed because parent method takes care of it
            using (var stream = new StreamWriter(s, Encoding.Default, 1024, true))
            using (JsonWriter writer = new JsonTextWriter(stream))
            {
                serializer.Serialize(writer, o);
            }
        }

        public object Deserialize(Stream s, Type t)
        {
            using (var reader = new StreamReader(s))
            using (var jsonReader = new JsonTextReader(reader))
            {
                return serializer.Deserialize(jsonReader, t);
            }
        }

        public string GetPathWithExtension(string path)
        {
            return Path.ChangeExtension(path, Extension);
        }

        #endregion
    }
}