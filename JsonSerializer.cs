﻿namespace RobinBird.Persistence.Json
{
    using System;
    using System.IO;
    using System.Text;
    using Abstractions.Serialization;
    using Newtonsoft.Json;

    public class JsonSerializer : DefaultSerializer
    {
        private readonly Newtonsoft.Json.JsonSerializer serializer;

        public JsonSerializer()
        {
            var settings = new JsonSerializerSettings();
            settings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;
            serializer = Newtonsoft.Json.JsonSerializer.CreateDefault(settings);
        }


        /// <summary>
        /// Extension of the file to serialize or deserialize. Returns pure extension without dot.
        /// (e.g. "xml", "json", etc.)
        /// </summary>
        public override string Extension
        {
            get { return "json"; }
        }

        /// <summary>
        /// Http style content type that this serializer accepts and creates.
        /// </summary>
        public override string HttpContentType
        {
            get { return "application/newtonsoft-json"; }
        }

        /// <summary>
        /// Serialize data to disk.
        /// </summary>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <param name="type">The type of the data to serialize.</param>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is null or empty.</exception>
        public override void SerializeToFile(string path, object o, Type type)
        {
            string pathWithExtension = AddExtensionToPath(path);
            CreateDirectoryForPath(pathWithExtension);

            using (var fileStream = new FileStream(pathWithExtension, FileMode.OpenOrCreate))
            using (var streamWriter = new StreamWriter(fileStream))
            using (JsonWriter jsonWriter = new JsonTextWriter(streamWriter))
            {
                serializer.Serialize(jsonWriter, o);
            }
        }

        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// </summary>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <param name="type">Type of the data to deserialize.</param>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null/empty or does not exist.</exception>
        public override object DeserializeFromFile(string path, Type type)
        {
            string pathWithExtension = AddExtensionToPath(path);

            using (var fileStream = new FileStream(pathWithExtension, FileMode.Open))
            using (var streamReader = new StreamReader(fileStream))
            using (var jsonReader = new JsonTextReader(streamReader))
            {
                return serializer.Deserialize(jsonReader, type);
            }
        }

        /// <summary>
        /// Serialize instance <paramref name="o" /> of type <paramref name="type" /> to text.
        /// </summary>
        public override string SerializeToString(object o, Type type)
        {
            var sb = new StringBuilder();
            using (var stringWriter = new StringWriter(sb))
            using (var jsonWriter = new JsonTextWriter(stringWriter))
            {
                serializer.Serialize(jsonWriter, o, type);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Deserialize text to to instance of type <paramref name="type" />.
        /// </summary>
        public override object DeserializeFromString(string data, Type type)
        {
            object obj;
            using (var stringReader = new StringReader(data))
            using (var jsonReader = new JsonTextReader(stringReader))
            {
                obj = serializer.Deserialize(jsonReader, type);
            }
            return obj;
        }
    }
}