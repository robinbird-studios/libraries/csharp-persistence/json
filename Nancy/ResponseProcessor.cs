﻿#region Disclaimer
// <copyright file="ResponseProcessor.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_NANCY

namespace RobinBird.Persistence.Json.Nancy
{
    using System;
    using System.Collections.Generic;
    using global::Nancy;
    using global::Nancy.Responses.Negotiation;

    public class ResponseProcessor : IResponseProcessor
    {
        private static bool IsWildcardProtobufContentType(MediaRange requestedContentType)
        {
            if (!requestedContentType.Type.IsWildcard &&
                string.Equals(Constants.NewtonsoftJsonFirstType, requestedContentType.Type, StringComparison.InvariantCultureIgnoreCase) == false)
            {
                return false;
            }

            if (requestedContentType.Subtype.IsWildcard)
            {
                return true;
            }

            string subtypeString = requestedContentType.Subtype.ToString();

            return subtypeString.EndsWith("+" + Constants.NewtonsoftJsonSubType, StringComparison.InvariantCultureIgnoreCase);
        }

        #region IResponseProcessor Implementation
        /// <summary>
        /// Determines whether the processor can handle a given content type and model.
        /// </summary>
        /// <param name="requestedMediaRange">Content type requested by the client.</param>
        /// <param name="model">The model for the given media range.</param>
        /// <param name="context">The nancy context.</param>
        /// <returns>
        /// A <see cref="T:Nancy.Responses.Negotiation.ProcessorMatch" /> result that determines the priority of the processor.
        /// </returns>
        public ProcessorMatch CanProcess(MediaRange requestedMediaRange, dynamic model, NancyContext context)
        {
            if (IsWildcardProtobufContentType(requestedMediaRange) || requestedMediaRange.Matches(Constants.NewtonsoftJsonContentType))
            {
                return new ProcessorMatch
                {
                    ModelResult = MatchResult.DontCare,
                    RequestedContentTypeResult = MatchResult.ExactMatch
                };
            }

            return new ProcessorMatch
            {
                ModelResult = MatchResult.DontCare,
                RequestedContentTypeResult = MatchResult.NoMatch
            };
        }

        /// <summary>
        /// Process the response.
        /// </summary>
        /// <param name="requestedMediaRange">Content type requested by the client.</param>
        /// <param name="model">The model for the given media range.</param>
        /// <param name="context">The nancy context.</param>
        /// <returns>
        /// A <see cref="T:Nancy.Response" /> instance.
        /// </returns>
        public global::Nancy.Response Process(MediaRange requestedMediaRange, dynamic model, NancyContext context)
        {
            return new Response(model);
        }

        /// <summary>
        /// Gets a set of mappings that map a given extension (such as .json)
        /// to a media range that can be sent to the client in a vary header.
        /// </summary>
        public IEnumerable<Tuple<string, MediaRange>> ExtensionMappings
        {
            get { return new[] {Tuple.Create(Constants.NewtonsoftJsonSubType, new MediaRange(Constants.NewtonsoftJsonContentType))}; }
        }
        #endregion
    }
}

#endif