﻿#region Disclaimer
// <copyright file="Serializer.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_NANCY

namespace RobinBird.Persistence.Json.Nancy
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using global::Nancy;
    using Newtonsoft.Json;

    public class Serializer : ISerializer
    {
        private static JsonSerializer _serializer;

        private static Serializer _instance;

        public Serializer()
        {
            if (_instance != null)
            {
                throw new InvalidOperationException("Use the singleton.");
            }
            _instance = this;
        }

        public static JsonSerializer SerializerInstance
        {
            get
            {
                if (_serializer == null)
                {
                    _serializer = JsonSerializer.CreateDefault();
                }
                return _serializer;
            }
        }

        public static Serializer Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Serializer();
                }
                return _instance;
            }
        }

        #region ISerializer Implementation
        /// <summary>
        /// Whether the serializer can serialize the content type
        /// </summary>
        /// <param name="contentType">Content type to serialize</param>
        /// <returns>
        /// True if supported, false otherwise
        /// </returns>
        public bool CanSerialize(string contentType)
        {
            if (string.IsNullOrEmpty(contentType))
            {
                return false;
            }

            string contentMimeType = contentType.Split(';')[0];

            return contentMimeType.Equals("application/newtonsoft-json", StringComparison.InvariantCultureIgnoreCase) ||
                   contentMimeType.Equals("text/json", StringComparison.InvariantCultureIgnoreCase) ||
                   (contentMimeType.StartsWith("application/vnd", StringComparison.InvariantCultureIgnoreCase) &&
                    contentMimeType.EndsWith("+json", StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Serialize the given model with the given contentType
        /// </summary>
        /// <param name="contentType">Content type to serialize into</param>
        /// <param name="model">Model to serialize</param>
        /// <param name="outputStream">Output stream to serialize to</param>
        /// <returns>
        /// Serialized object
        /// </returns>
        public void Serialize<TModel>(string contentType, TModel model, Stream outputStream)
        {
            using (var writer = new StreamWriter(outputStream))
            using (JsonWriter jsonWriter = new JsonTextWriter(writer))
            {
                SerializerInstance.Serialize(jsonWriter, model, typeof (TModel));
            }
        }

        /// <summary>
        /// Gets the list of extensions that the serializer can handle.
        /// </summary>
        /// <value>
        /// An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of extensions if any are available, otherwise an empty
        /// enumerable.
        /// </value>
        public IEnumerable<string> Extensions
        {
            get { yield return "json"; }
        }
        #endregion
    }
}

#endif