﻿#region Disclaimer
// <copyright file="ResponseFormatterExtensions.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_NANCY

namespace RobinBird.Persistence.Json.Nancy
{
    using global::Nancy;

    public static class ResponseFormatterExtensions
    {
        public static global::Nancy.Response AsNewtonsoftJson<TModel>(this IResponseFormatter formatter, TModel model, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            var newtonsoftJsonResponse = new Response(model);
            newtonsoftJsonResponse.StatusCode = statusCode;
            return newtonsoftJsonResponse;
        }
    }
}

#endif