﻿#region Disclaimer
// <copyright file="Response.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_NANCY

namespace RobinBird.Persistence.Json.Nancy
{
    using System;
    using System.IO;
    using global::Nancy;

    public class Response : global::Nancy.Response
    {
        public Response(object model)
        {
            Contents = model == null ? NoBody : GetJsonContents(model, Serializer.Instance);
            ContentType = Constants.NewtonsoftJsonContentType;
            StatusCode = HttpStatusCode.OK;
        }

        private static Action<Stream> GetJsonContents(object model, ISerializer serializer)
        {
            return stream => serializer.Serialize(Constants.NewtonsoftJsonContentType, model, stream);
        }
    }
}

#endif