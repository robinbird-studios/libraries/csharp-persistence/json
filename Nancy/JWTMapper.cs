﻿#region Disclaimer
// <copyright file="JWTMapper.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_NANCY

namespace RobinBird.Persistence.Json.Nancy
{
    using System.Globalization;
    using System.IO;
    using System.Text;
    using Jose;
    using Newtonsoft.Json;

    public class JWTMapper : IJsonMapper
    {
        #region IJsonMapper Implementation
        public string Serialize(object obj)
        {
            string json;
            using (var stringWriter = new StringWriter(new StringBuilder(256), CultureInfo.InvariantCulture))
            using (var jsonTextWriter = new JsonTextWriter(stringWriter))
            {
                jsonTextWriter.Formatting = Serializer.SerializerInstance.Formatting;
                Serializer.SerializerInstance.Serialize(jsonTextWriter, obj);
                json = stringWriter.ToString();
            }
            return json;
        }

        public T Parse<T>(string json)
        {
            T obj;
            using (var stringReader = new StringReader(json))
            using (var jsonTextReader = new JsonTextReader(stringReader))
            {
                obj = Serializer.SerializerInstance.Deserialize<T>(jsonTextReader);
            }
            return obj;
        }
        #endregion
    }
}

#endif