﻿#region Disclaimer
// <copyright file="Constants.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_NANCY

namespace RobinBird.Persistence.Json.Nancy
{
    public static class Constants
    {
        public const string NewtonsoftJsonFirstType = "application";
        public const string NewtonsoftJsonSubType = "newtonsoft-json";
        public const string NewtonsoftJsonContentType = NewtonsoftJsonFirstType + "/" + NewtonsoftJsonSubType;
    }
}

#endif