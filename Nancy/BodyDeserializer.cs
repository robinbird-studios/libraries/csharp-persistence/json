﻿#region Disclaimer
// <copyright file="BodyDeserializer.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion


#if PERSISTENCE_NANCY

namespace RobinBird.Persistence.Json.Nancy
{
    using System.IO;
    using global::Nancy.ModelBinding;
    using Newtonsoft.Json;

    public class BodyDeserializer : IBodyDeserializer
    {
        #region IBodyDeserializer Implementation
        /// <summary>
        /// Whether the deserializer can deserialize the content type
        /// </summary>
        /// <param name="contentType">Content type to deserialize</param>
        /// <param name="context">Current <see cref="T:Nancy.ModelBinding.BindingContext" />.</param>
        /// <returns>
        /// True if supported, false otherwise
        /// </returns>
        public bool CanDeserialize(string contentType, BindingContext context)
        {
            if (string.IsNullOrEmpty(contentType))
            {
                return false;
            }

            return contentType.Contains(Constants.NewtonsoftJsonContentType);
        }

        /// <summary>
        /// Deserialize the request body to a model
        /// </summary>
        /// <param name="contentType">Content type to deserialize</param>
        /// <param name="bodyStream">Request body stream</param>
        /// <param name="context">Current <see cref="T:Nancy.ModelBinding.BindingContext" />.</param>
        /// <returns>
        /// Model instance
        /// </returns>
        public object Deserialize(string contentType, Stream bodyStream, BindingContext context)
        {
            object deserializedBody;

            using (var reader = new StreamReader(bodyStream))
            using (JsonReader jsonReader = new JsonTextReader(reader))
            {
                deserializedBody = Serializer.SerializerInstance.Deserialize(jsonReader, context.DestinationType);
            }

            return deserializedBody;
        }
        #endregion
    }
}

#endif